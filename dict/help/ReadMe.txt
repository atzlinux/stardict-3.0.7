Try:
make stardict-html
sudo make install

make stardict-maintainer-clean


For example:
=====
cd zh_CN/
Use gedit to edit "zh_CN.po" file.
git add zh_CN.po
git rm -rf html
cd ..
make stardict-maintainer-clean
make stardict-html
cd zh_CN/
git add html
git commit -m "Update zh_CN html and po"
git push
=====
